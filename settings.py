TITLE = "Planet GNOME"
BASEURL = "https://felipeborges.pages.gitlab.gnome.org/planet.gnome.org/"
NUMBER_OF_POSTS = 50
POSTS_PER_FEED = 2
AVATAR_IMAGE_SIZE  = 200 # in pixels (max 512)
